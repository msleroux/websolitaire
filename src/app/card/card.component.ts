import { Component, OnInit, Input } from '@angular/core';
import { Card } from '../game/card';
import { PileType, StockClickType } from '../game/enum';
import { Data } from '../game/data';
import { Game } from '../game/game';
import { Pile } from '../game/pile';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  imagePath: string;
  cssClass: string;

  @Input() card: Card;

  isClicked: boolean;

  constructor() {
    this.isClicked = false;
  }

  ngOnInit() {
    if (this.card) {
      this.card.component = this;
    }

    // console.log("setting card class...");
    this.setClass();
    // console.log("setting card image...");
    this.setImage();
    // this.logCardDetail();
  }

  setImage() {
    this.imagePath = Game.getImagePath(this.card);
  }

  logCardDetail() {
    console.log('card: ' + this.card.displayName + ', canClick=' + this.card.canClick);
  }

  sayHi() {
    // this.logCardDetail();
    alert(this.card.displayName);
  }

  processCardClick(): void {
    // this.logCardDetail();
    // console.log("processCardClick: card=" + this.card.displayName);
    this.card.pile.component.wasClickHandled = true; // prevent pile component from also handling the click event

    if (this.card.isFaceUp) {
      this.processFaceUpCard();
    } else {
      this.processFaceDownCard();
    }
  }

  processFaceDownCard() {
    if (this.card.isTopCardInPile) {
      if (this.card.pile.pileType === PileType.Stock) {
        // TODO: Only do this if the stock should remain face down
        this.processNewCardSelection();
      } else {
        // Can only flip over a card at the top of the pile
        this.card.isFaceUp = true;
        this.setImage();
      }
    }
  }

  processFaceUpCard() {
    if (Data.selectedCard) {
      // Card is already selected, check if the selected card can be added to the newly clicked card
      this.processSecondCardClick();
    } else {
      // console.log(fn + "card not currently selected");
      if (this.card.canClick) {
        this.processNewCardSelection();
      }
    }
  }

  private processSecondCardClick() {
    const fn = 'processSecondCardClick: ';
    const log = false;

    const selectedCard = Data.selectedCard;
    if (log) {
      console.log(`${fn}highlighted card=${selectedCard.displayName}, card clicked now=${this.card.displayName}`);
    }

    if (selectedCard.id === this.card.id) {
      // Selected card was clicked again, unselect it
      if (log) {
        console.log(`${fn}highlighted card was clicked again, unselecting card`);
      }
      this.setSelectedCard(null);
    } else if (this.card.pile.checkIfCardCanBeAdded(selectedCard)) {// (Data.selectedCard.pile.checkIfCardCanBeAdded(this.card)) {
      this.moveSelectedCard();
    }
  }

  private processNewCardSelection() {
    const fn = 'processNewCardSelection: ';
    const log = false;
    const pileLastCardIndex = this.card.pile.cardCount - 1;

    if (log) {
      console.log(`${fn}${this.card.pile.pileTypeName} last card index=${pileLastCardIndex}, this card index=${this.card.indexInPile}`);
    }

    let canSelect = false;

    const cardPile = this.card.pile;
    if (this.card.indexInPile === pileLastCardIndex) {
      // Card on top of pile was clicked
      if (cardPile.pileType === PileType.Stock) {
        this.processStockCardClick();
      } else {
        canSelect = true;
      }
    } else if (cardPile.allowBuild) {
      // Check if selected card is part of group of cards below it
      canSelect = this.checkMidPileSelection();
      if (!canSelect && log) {
        console.log(`${fn}mid-pile card (${this.card.displayName}) not part of group`);
      }
    }

    if (canSelect) {
      this.setSelectedCard(this.card);
      if (Data.selectedCard && log) {
        console.log(`${fn}newly selected card=${Data.selectedCard.displayName}`);
      }
    }
  }

  private processStockCardClick() {
    const stockPile = this.card.pile;
    const clickType = Data.game.stockClick;

    if (clickType === StockClickType.AddOneToEachTableau) {
      // console.log('processStockCardClick: moving one card to each tableau');
      for (let t = 0; t < Data.tableaus.length; t++) {
        let tableau = Data.tableaus[t];
        stockPile.moveTopCard(tableau, true);
        tableau.component.prepare();
      }
    }
    else if (clickType === StockClickType.AddOneToWaste || clickType === StockClickType.AddThreeToWaste) {
      // Stock card should automatically be moved to waste pile
      stockPile.component.prepare();
      Data.waste.component.prepare();
    }
  }

  private checkMidPileSelection(): boolean {
    let allPartOfGroup = true;

    const pileCards = this.card.pile.cards;
    const pileLastCardIndex = pileCards.length - 1;

    let index: number = this.card.indexInPile;
    while (index < pileLastCardIndex) {
      if (!this.card.pile.checkIfCardsArePartOfGroup(pileCards[index], pileCards[index + 1])) {
        allPartOfGroup = false;
        break;
      }
      index++;
    }

    return allPartOfGroup;
  }

  private setSelectedCard(card: Card) {
    if (card) {
      this.isClicked = !this.isClicked;
    } else {
      this.isClicked = false;
    }
    Data.selectedCard = card;
    this.setClass();
  }

  setClass(): void {
    this.cssClass = this.isClicked ? 'cardHighlight' : 'cardDefault';
  }

  moveSelectedCard(targetPile: Pile = null) {
    const fn = 'moveSelectedCard: ';
    const log = false;

    this.isClicked = false;
    this.setClass();

    // Move selected card to the pile of the card that was clicked after the first card was selected
    const sourcePile: Pile = Data.selectedCard.pile;

    if (!targetPile) {
      targetPile = this.card.pile;
    }

    let firstCardIndex = -1;
    if (Data.selectedCard.indexInPile < (Data.selectedCard.pile.lastCardIndex)) {
      firstCardIndex = Data.selectedCard.indexInPile;
    }

    if (log) {
      const what: string = (firstCardIndex >= 0) ? `cards (from index ${firstCardIndex})` : 'top card';
      console.log(`${fn}moving ${what} from ${sourcePile.pileTypeName}[${sourcePile.pileIndex}] to ${targetPile.pileTypeName}[${targetPile.pileIndex}]`);
    }

    if (firstCardIndex >= 0) {
      sourcePile.moveCardGroup(targetPile, firstCardIndex, true);
    } else {
      sourcePile.moveTopCard(targetPile, true);
    }
    sourcePile.ensureTopCardFaceUp();

    Data.selectedCard = null;

    // Refresh piles
    // console.log(`${fn}preparing target (${targetPile.pileTypeName})`);
    targetPile.component.prepare();
    // console.log(`${fn}preparing source (${sourcePile.pileTypeName})`);
    sourcePile.component.prepare();
  }
}
