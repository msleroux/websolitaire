import { Component, OnInit } from '@angular/core';
import { Data } from '../game/data';
import { GameName } from '../game/enum';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  // menuItems: string[];
  games = GameName;
  // names: string[];
  id: string;
  ids: string[] = [];
  gameName = 'Card game';
  menuItemsVisible = false;
  cssClass: string;

  name(id: string): string {
    let value = GameName[id];
    return value.replace(/_/g, ' ');
  }

  constructor() {
    let gameIds = Object.keys(GameName);
    for (let id of gameIds ) {
      if (!isNaN(parseInt(id))) {
        this.ids.push(id);
      }
     }

    Data.menuComponent = this;
  }

  ngOnInit() {
    this.prepare();
  }

  prepare() {
    this.menuItemsVisible = false;
    this.setClass();

    if (Data.game) {
      this.gameName = Data.game.name;
    }

    // console.log(`menu prepare: gameName=${this.gameName}`);
  }

  toggle() {
    this.menuItemsVisible = !this.menuItemsVisible;
    this.setClass();
  }

  setClass() {
    const display = this.menuItemsVisible ? 'visible' : 'hidden';
    this.cssClass = `menuPanel ${display}`;
  }
}
