import { Component, OnInit } from '@angular/core';
import { PileType } from '../game/enum';
import { Data } from '../game/data';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {
  pileType = PileType; // needed in HTML to set attributes
  wasteVisible = true;

  constructor() { }

  ngOnInit() {
    if (Data.game) {
      this.wasteVisible = Data.game.wasteVisible;
    }
  }

}
