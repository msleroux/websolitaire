import { Data } from './data';
import { Card } from './card';
import { CardSuit, CardRank, NextCardRank, NextCardSuit } from './enum';
import { PileType, PileMoveType, PileFanType, StockClickType, StockEmptyType, TableauEmptyType } from './enum';
import { Pile } from './pile';

export class Game {
    deckCount = 1;

    private _name: string;

    tableauCount: number;
    tableauNextCardRank: NextCardRank = NextCardRank.Lower;
    tableauNextCardSuit: NextCardSuit = NextCardSuit.AlternateColour;
    tableauEmpty: TableauEmptyType = TableauEmptyType.AllowAny;
    tableauFanStyle: PileFanType = PileFanType.Vertical;
    tableauCardMove: PileMoveType = PileMoveType.TopCardOnly;
    tableauAllowBuild = true;

    reserveCount: number;

    foundationCount = 4;
    foundationRank: CardRank = CardRank.Ace;
    foundationNextCardRank: NextCardRank = NextCardRank.Higher;
    foundationNextCardSuit: NextCardSuit = NextCardSuit.Match;
    foundationMustGetCardAtStart = false;

    stockClick: StockClickType = StockClickType.AddOneToWaste;
    stockEmpty: StockEmptyType = StockEmptyType.MoveFromWaste;
    stockTopCardFaceUp = true;

    wasteVisible = true;
    wasteFan: PileFanType = PileFanType.None;

    get name(): string {
        return this._name;
    }

    constructor(name: string) {
        this._name = name;
        Data.clear();
    }

    static getImagePath (card: Card): string {
        if (card) {
            const subPath: string = card.isFaceUp ? `${card.suitName}/${card.value}` : 'back';
            const path = `assets/card/${subPath}.png`;
            return path.toLowerCase();
        }
        return '';
    }

    start() {
        // console.log(`starting ${this._name}...`);
        // this.prepare();
        this.createDeck();

        if (this.deckCount > 0 && this.foundationCount === 4) {
            // TODO: Cater for different foundation ranks
            this.foundationCount = 4 * this.deckCount;
        }

        // if (!Data.wasPrepared) {
            this.prepareGamePiles();
            // Data.wasPrepared = true;
        // }

        this.dealCards();
        // console.log(`finished ${this._name}`);
    }

    private dealCards(): void {
        if (this.foundationMustGetCardAtStart) {
            this.dealFoundationCards();
        }
        this.dealTableauCards();
        this.dealRemainingCardsToStock();
    }

    private dealFoundationCards() {
        // TODO: Extract foundation cards from deck and deal each to foundations
    }

    protected dealCardToTableaus(cardsPerTableau = 1, mustTopCardBeFaceUp = true, firstPileIndex = 0, numberOfTableaus = -1) {
        // console.log(`dealCardToTableaus: cards=${cardsPerTableau}, firstIndex=${firstPileIndex}, nr=${numberOfTableaus}`);

        // Determine how many face-down cards to deal first
        const faceDownCount = mustTopCardBeFaceUp ? cardsPerTableau - 1 : cardsPerTableau;

        let faceUp = false;
        // Deal face-down cards
        if (faceDownCount > 0) {
            this.dealCardToPiles(Data.tableaus, firstPileIndex, numberOfTableaus, faceDownCount, faceUp);
        }

        if (mustTopCardBeFaceUp) {
            // Deal face-up cards
            faceUp = true;
            this.dealCardToPiles(Data.tableaus, firstPileIndex, numberOfTableaus, 1, faceUp);
        }
    }

    private dealCardToPiles(piles: Pile[], firstPileIndex: number, numberOfPiles: number, cardsPerPile: number, mustBeFaceUp: boolean) {
        // Sometimes all the tableaus get a card, then later only some of them do, e.g. with Spider
        const deck = Data.deck;

        // Check that last pile index doesn't exceed actual last pile index, or set last pile index if unspecified
        let lastPileIndex = numberOfPiles - 1;
        const lastIndex = (piles.length - 1);
        if (lastPileIndex > lastIndex || lastPileIndex < 0) {
            lastPileIndex = lastIndex;
        }

        // console.log(`dealCardToPiles: firstIndex=${firstPileIndex}, lastIndex=${lastPileIndex}, nrPiles=${numberOfPiles}, cards=${cardsPerPile}`);

        for (let cardIndex = 0; cardIndex < cardsPerPile; cardIndex++) {
            for (let pileIndex = firstPileIndex; pileIndex <= lastPileIndex; pileIndex++) {
                // console.log(`dealCardToPiles: cardIndex=${cardIndex}, pileIndex=${pileIndex}`);
                deck.moveCard (cardIndex, piles[pileIndex], mustBeFaceUp);
                if (!deck.hasCards) {
                    break;
                }
            }
            if (!deck.hasCards) {
                break;
            }
        }
    }

    private dealRemainingCardsToStock() {
        let faceUp = false;
        const deck = Data.deck;

        const remainingCards = this.stockTopCardFaceUp ? 1 : 0;
        while (deck.cards.length > remainingCards) {
            deck.moveTopCard(Data.stock, faceUp);
        }

        if (remainingCards > 0) {
            // Last stock card must be face up
            faceUp = true;
            deck.moveTopCard(Data.stock, faceUp);
        }
    }

    private prepareGamePiles(): void {
        // console.log("adding foundations...");
        this.addFoundations();
        // console.log("adding tableaus...");
        this.addTableaus();

        // console.log("instantiating stock...");
        Data.stock = new Pile(PileType.Stock);
        Data.stock.canClickCard = true;

        // console.log("instantiating waste...");
        Data.waste = new Pile(PileType.Waste);
        Data.waste.canClickCard = true;
        Data.waste.nextCardRank = NextCardRank.Any;
        Data.waste.nextCardSuit = NextCardSuit.Any;
        Data.waste.firstCardSuit = CardSuit.Any;
        Data.waste.fanStyle = this.wasteFan;
    }

    private createDeck(): void {
        Data.deck = new Pile(PileType.Deck);

        // Create a new deck
        for (let deckIndex = 0; deckIndex < this.deckCount; deckIndex++) {
            this.createCards(CardSuit.Club);
            this.createCards(CardSuit.Diamond);
            this.createCards(CardSuit.Spade);
            this.createCards(CardSuit.Heart);
        }

        this.shuffleDeck();
    }

    private createCards(suit: CardSuit): void {
        const cards = Data.deck.cards;
        for (let i = 1; i <= 13; i++) {
            const card: Card = new Card(cards.length, i, suit);
            card.isFaceUp = false;
            cards.push(card);
        }
    }

    private shuffleDeck(): void {
        const temp: Pile = new Pile(PileType.Deck);
        let index: number;

        const faceUp = false;

        const deck = Data.deck;
        const cards = deck.cards;
        while (cards.length > 1) {
            // Identify a random card in the remaining deck
            index = Math.floor(Math.random() * cards.length);
            deck.moveCard(index, temp, faceUp);
        }

        deck.moveCard(0, temp, faceUp);

        // Deck is now shuffled
        Data.deck = temp;
    }

    private addTableaus() {
        for (let i = 0; i < this.tableauCount; i++) {
            const pile = new Pile(PileType.Tableau);
            pile.firstCardSuit = CardSuit.Any;
            pile.moveStyle = this.tableauCardMove;
            pile.fanStyle = this.tableauFanStyle;
            pile.nextCardRank = this.tableauNextCardRank;
            pile.nextCardSuit = this.tableauNextCardSuit;
            pile.canClickCard = true;
            pile.allowBuild = this.tableauAllowBuild;
            this.addPile(Data.tableaus, pile);
        }
    }

    private addFoundations() {
        for (let i = 0; i < this.foundationCount; i++) {
            const pile = new Pile(PileType.Foundation);
            pile.firstCardValue = this.foundationRank;
            pile.firstCardSuit = CardSuit.Any;
            pile.nextCardRank = this.foundationNextCardRank;
            pile.nextCardSuit = this.foundationNextCardSuit;
            this.addPile(Data.foundations, pile);
        }
    }

    private addPile(target: Pile[], pile: Pile) {
        pile.pileIndex = target.length;
        target.push(pile);
    }

    protected dealTableauCards() { }
}
