export enum StockClickType {
    None,
    AddOneToWaste,
    AddThreeToWaste,
    AddOneToEachTableau,
    AddOneToFreeReserves,
    ToBeDetermined
}

export enum StockEmptyType {
    Final,
    MoveFromWaste
}

export enum TableauEmptyType {
    None,
    StayEmpty,
    AllowAny,
    King,
    Autofill
}

export enum NextCardRank {
    Any,
    Higher,
    Lower,
    HigherOrLower
}

export enum NextCardSuit {
    Any,
    AlternateColour,
    Match
}

export enum PileType {
    Deck,
    Foundation,
    Reserve,
    Stock,
    Tableau,
    Waste
}

export enum PileMoveType {
    None,
    Any,
    TopCardOnly,
    Group
}

export enum PileFanType {
    None,
    Horizontal,
    Grid,
    Vertical,
    TopThreeHorizontal // the top three cards will be fanned out horizontally
}

export enum CardSuit {
    None,
    Club,
    Diamond,
    Heart,
    Spade,
    Any
}

export enum CardRank {
    None,
    Ace,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King
}

export enum GameName {
    Klondike,
    Spider
    // Aces_Up
}
