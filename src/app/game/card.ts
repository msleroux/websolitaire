import { Pile } from './pile';
import { CardComponent } from '../card/card.component';
import { CardSuit, CardRank } from './enum';

export class Card {
    private _id = -1;
    private _value: number;
    private _suit: CardSuit;
    private _suitName: string;
    private _displayName = '';

    isFaceUp = false;
    pile: Pile; // the pile that currently contains the card
    canClick = true;
    cssStyle: object;
    indexInPile = -1;
    component: CardComponent;

    get id(): number {
        return this._id;
    }

    get value(): number {
        return this._value;
    }

    get suit(): CardSuit {
        return this._suit;
    }

    get suitName(): string {
        return this._suitName;
    }

    get displayName(): string {
        return this._displayName;
    }

    get isBlack(): boolean {
        return this._suit === CardSuit.Club || this._suit === CardSuit.Spade;
    }

    get isTopCardInPile(): boolean {
        if (this.pile && this.indexInPile >= 0) {
            // Card belongs to a pile, check if it's the last card in the pile
            return (this.indexInPile === this.pile.lastCardIndex);
        }
        return false;
    }

    constructor(id: number, value: number, suit: CardSuit) {
        this._id = id;
        this._value = value;
        this._suit = suit;
        this._suitName = CardSuit[this.suit];
        this._displayName = `${CardRank[this.value]} of ${this.suitName}s`;
    }
}

