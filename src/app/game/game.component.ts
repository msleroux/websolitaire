import { Component, OnInit } from '@angular/core';
import { Game } from './game';
import { Data } from './data';
import { PileType, GameName } from './enum';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  game: Game;
  pileType = PileType;

  constructor(private route: ActivatedRoute) {
    // console.log('new game component');
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        // const name = params['name'];
        const id: number = params['id'];

        Data.newGame(id);
        Data.menuComponent.prepare();
        this.game = Data.game;
        this.game.start();
      }
   );
  }
}
