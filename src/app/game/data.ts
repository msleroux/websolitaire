import { Game } from './game';
import { Klondike } from './games/klondike';
import { Spider } from './games/spider';
import { Card } from './card';
import { Pile } from './pile';
import { MenuComponent } from '../menu/menu.component';
import { GameName } from './enum';

export class Data {
    static foundations: Pile[] = [];
    static tableaus: Pile[] = [];
    static reserves: Pile[] = [];
    static stock: Pile;
    static waste: Pile;
    static deck: Pile;

    static game: Game;

    static wasPrepared = false;

    static selectedCard: Card;

    static menuComponent: MenuComponent;

    static clear() {
        this.wasPrepared = false;
        this.selectedCard = null;
        this.clearPileGroup(this.foundations);
        this.clearPileGroup(this.tableaus);
        this.clearPileGroup(this.reserves);
    }

    private static clearPileGroup(pile: Pile[]) {
        pile.splice(0, pile.length);
    }

    public static newGame(id: number) {
        // To convert a number to an enum member you have to first get the member name
        // For some reason you cannot directly get the enum member
        const name = GameName[id];
        const game: GameName = GameName['' + name];

        switch (game) {
            case GameName.Klondike:
                this.game = new Klondike();
                break;
            case GameName.Spider:
                this.game = new Spider();
                break;
            default:
                console.log(`game unknown - id=${id}, name=${name}`);
                break;
        }
    }
}
