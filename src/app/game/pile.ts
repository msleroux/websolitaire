import { Card } from './card';
import { CardSuit, NextCardRank, NextCardSuit, PileType, PileMoveType, PileFanType } from './enum';
import { PileComponent } from '../pile/pile.component';

export class Pile {
    private _pileType: PileType;
    private _pileTypeName: string;

    pileIndex = 0;

    firstCardValue = 0; // any value card allowed
    firstCardSuit: CardSuit = CardSuit.None; // set this to Suit.Any to allow any card, or a specific Suit to limit the first card

    nextCardRank: NextCardRank = NextCardRank.Any;
    nextCardSuit: NextCardSuit = NextCardSuit.Any;

    moveStyle: PileMoveType = PileMoveType.None;
    fanStyle: PileFanType = PileFanType.None;
    allowBuild = false;

    canClickCard = false;

    foundationStartValue = 0;

    component: PileComponent;

    indexOfFirstCardAdded = -1; // used mainly with partial fans

    cards: Card[] = [];

    get pileType(): PileType {
        return this._pileType;
    }

    get pileTypeName(): string {
        return this._pileTypeName;
    }

    get hasCards(): boolean {
        return this.cards && this.cards.length > 0;
    }

    get cardCount(): number {
        return (this.cards) ? this.cards.length : 0;
    }

    get lastCardIndex(): number {
        return this.cardCount - 1;
    }

    constructor(type: PileType) {
        this._pileType = type;
        this._pileTypeName = PileType[type];
    }

    moveTopCard(target: Pile, setFaceUp: boolean = true, numberToMove: number = 1): void {
        // Move the top card of this pile to the target pile
        // If numberToMove is greater than 1, the next top card is moved
        if (this.lastCardIndex >= 0) {
            target.indexOfFirstCardAdded = target.cardCount;
            if (numberToMove <= 0) {
                numberToMove = 1;
            }

            for (let i = 0; i < numberToMove; i++) {
                this.moveCard(this.lastCardIndex, target, setFaceUp);
                if (this.cardCount === 0) {
                    // No more cards
                    break;
                }
            }
        }
    }

    moveCardGroup(target: Pile, firstCardIndex: number, setFaceUp: boolean = true): void {
        // Move a group of cards to the target pile, starting with the card at firstCardIndex
        const index: number = firstCardIndex;

        target.indexOfFirstCardAdded = target.cardCount;
        while (index < this.cards.length) {
            this.moveCard(index, target, setFaceUp);
            // Don't increment index - the card array length will get one less per loop
        }
    }

    getTopCard(): Card {
        if (this.lastCardIndex >= 0) {
            return this.cards[this.lastCardIndex];
        }
        return null;
    }

    moveCard(cardIndex: number, target: Pile, setFaceUp: boolean = true): void {
        // Add card to target pile
        const fn = 'moveCard: ';

        const card: Card = this.cards[cardIndex];
        card.pile = target;
        card.cssStyle = null;

        // var oldFaceUp: boolean = card.isFaceUp;
        card.isFaceUp = setFaceUp;

        // console.log(`${fn}Moving ${card.displayName} to ${target.pileTypeName}[${target.pileIndex}]`);
        card.canClick = target.canClickCard;
        card.indexInPile = target.cards.length;

        target.cards.push(card);
        this.removeCard(cardIndex);
    }

    removeCard(index: number): void {
        this.cards.splice(index, 1);
    }

    clear(): void {
        this.cards.splice(0, this.cards.length);
    }

    checkIfCardCanBeAdded(card: Card): boolean {
        const fn = `checkIfCardCanBeAdded (to ${this._pileTypeName}): `;
        const log = false;

        // TODO: Check source pile
        if (this._pileType === PileType.Stock) {
            if (log) {
                console.log(`${fn}can't add card`);
            }
            return false;
        }

        if (this._pileType === PileType.Waste) {
            // Cards can only be added to waste pile from stock
            return card.pile.pileType === PileType.Stock;
        }

        const topCard = this.getTopCard();
        if (topCard) {
            if (log) {
                console.log(`${fn}checking if ${card.displayName} can be added to ${topCard.displayName}`);
            }
            return this.checkIfCardsArePartOfGroup(topCard, card);
        }
        else {
            // Pile has no cards, check if card can be added
            if (log) {
                console.log(`${fn}checking if card can be added to empty pile`);
            }
            return this.checkIfCardCanBeAddedToEmptyPile(card);
        }

        if (log) {
            console.log(`${fn}no matching conditions, card can't be added`);
        }

        return false;
    }

    checkIfCardsArePartOfGroup(underCard: Card, overCard: Card): boolean {
        let areGroup = false;

        if (this.checkCardGrouping_Suit(underCard, overCard)) {
            areGroup = this.checkCardGrouping_Rank(underCard.value, overCard.value);
        }

        if (!areGroup) {
            console.log(`checkIfCardsArePartOfGroup: result=false, underCard=${underCard.displayName}, overCard=${overCard.displayName}`);
        }

        return areGroup;
    }

    private checkCardGrouping_Suit(underCard: Card, overCard: Card): boolean {
        // If 9 was on 10, the 10 would be the underCard, the 9 would be the overCard
        let canAdd = true;
        switch (this.nextCardSuit) {
            case NextCardSuit.AlternateColour:
                canAdd = underCard.isBlack !== overCard.isBlack;
                break;
            case NextCardSuit.Match:
                canAdd = (underCard.suit === overCard.suit);
                break;
        }

        // console.log(`checkCardGrouping_Suit: nextCardSuit=${NextCardSuit[this.nextCardSuit]}, topCard.isBlack=${topCard.isBlack}, newCard.isBlack=${newCard.isBlack}, canAdd=${canAdd}`);
        return canAdd;
    }

    private checkCardGrouping_Rank(underCardValue: number, overCardValue: number): boolean {
        let canAdd = false;

        if (this.nextCardRank === NextCardRank.HigherOrLower) {   // Check if the card is next higher or lower in sequence
            if ((overCardValue === (underCardValue + 1)) || (overCardValue === (underCardValue - 1))) {
                canAdd = true;
            }
        } else {
            let rankDifference = 1;
            if (this.nextCardRank === NextCardRank.Lower) {
                rankDifference = -1;
            } // next card must be lower in rank
            const nextCardExpectedValue: number = underCardValue + rankDifference;
            if (overCardValue === nextCardExpectedValue) {
                canAdd = true;
            }
        }

        // console.log(`checkCardGrouping_Rank: nextCardRank=${NextCardRank[this.nextCardRank]}, underCardValue=${underCardValue}, overCardValue=${overCardValue}, canAdd=${canAdd}`);
        return canAdd;
    }

    checkIfCardCanBeAddedToEmptyPile(card: Card): boolean {
        const fn = 'checkIfCardCanBeAddedToEmptyPile: ';

        const log = false;
        // if (this.pileType === PileType.Waste) {
        //     log = true;
        // }

        if (log) {
            console.log(`${fn}card.value=${card.value}, firstCardSuit=${CardSuit[this.firstCardSuit]}, firstCardValue=${this.firstCardValue}`);
        }

        if (this.firstCardSuit === CardSuit.None) {
            // No card allowed
            if (log) {
                console.log(`${fn}no card allowed on ${this.pileTypeName}`);
            }
            return false;
        }

        const isAnyValueOk: boolean = (this.firstCardValue <= 0);
        const isAnySuitOk: boolean = (this.firstCardSuit === CardSuit.Any);

        if (isAnyValueOk && isAnySuitOk) {
            // Any card allowed
            if (log) {
                console.log(fn + 'any card OK');
            }
            return true;
        }

        if (isAnyValueOk || this.firstCardValue === card.value) {   // Card's value is acceptable
            if (isAnySuitOk || this.firstCardSuit === card.suit) {
                // Card's suit is acceptable
                if (log) {
                    console.log(fn + 'card acceptable');
                }
                return true;
            }
        }

        if (log) {
            console.log(fn + 'card not acceptable');
        }
        return false;
    }

    ensureTopCardFaceUp() {
        const card: Card = this.getTopCard();
        if (card) {
            card.isFaceUp = true;
            if (card.component) {
                card.component.setImage();
            }
        }
    }
}

