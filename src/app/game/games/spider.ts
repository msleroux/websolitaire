import { Data } from '../data';
import { Game } from '../game';
import { NextCardSuit, PileMoveType, StockClickType, StockEmptyType } from '../enum';

export class Spider extends Game {
    constructor() {
        super('Spider');
        this.deckCount = 2;

        this.tableauCount = 10;
        this.tableauNextCardSuit = NextCardSuit.Match;
        this.tableauCardMove = PileMoveType.Group;

        this.stockEmpty = StockEmptyType.Final;
        this.stockClick = StockClickType.AddOneToEachTableau;
        this.stockTopCardFaceUp = false;

        this.wasteVisible = false;
    }

    protected dealTableauCards() {
        // 6 cards are dealt to first four piles and 5 cards to the remaining ones; top card is face up
        let cardCount = 6;
        const tableauCount = 4;
        this.dealCardToTableaus(cardCount, true, 0, tableauCount);

        cardCount = 5;
        this.dealCardToTableaus(cardCount, true, tableauCount);
    }
}
