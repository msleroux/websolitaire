import { Data } from '../data';
import { Game } from '../game';
import { PileMoveType, PileFanType, StockClickType, StockEmptyType } from '../enum';

export class Klondike extends Game {
    constructor() {
        super('Klondike');

        this.tableauCount = 7;
        this.tableauCardMove = PileMoveType.Group;
        this.wasteFan = PileFanType.TopThreeHorizontal;

        this.stockTopCardFaceUp = false;
        this.stockClick = StockClickType.AddThreeToWaste;
        this.stockEmpty = StockEmptyType.MoveFromWaste;
    }

    protected dealTableauCards() {
        // super.dealTableauCards();
        let faceUp: boolean;
        for (let t = 0; t < this.tableauCount; t++) {
            // First card is face-up, remaining cards are face down
            // Deal cards in ascending amount per pile
            faceUp = true;
            for (let p: number = t; p < this.tableauCount; p++) {
                Data.deck.moveTopCard(Data.tableaus[p], faceUp);
                faceUp = false;
            }
            // console.log("Adding card to tableau[" + t + "], faceUp=" + faceUp);
        }
    }
}
