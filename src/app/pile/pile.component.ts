import { Component, OnInit, Input } from '@angular/core';
import { PileType, PileFanType, StockEmptyType, TableauEmptyType, StockClickType } from '../game/enum';
import { Pile } from '../game/pile';
import { Card } from '../game/card';
import { Data } from '../game/data';

@Component({
  selector: 'app-pile',
  templateUrl: './pile.component.html',
  styleUrls: ['./pile.component.css']
})
export class PileComponent implements OnInit {
  @Input() pile: Pile;
  @Input() pileType: PileType;

  cssClass: string;
  cssStyle: object;

  cards: Card[] = [];

  wasClickHandled = false;

  constructor() { }

  ngOnInit() {
    this.prepareInitialPile();
    this.prepare();

    if (this.pile && !this.pile.component) {
      this.pile.component = this;
    }
  }

  prepare(isRefresh: boolean = false) {
    const what: string = this.pile ? this.pile.pileTypeName : 'pile null';
    // console.log("prepare: " + what);
    if (this.pileType !== PileType.Deck && this.pile) {
      this.checkForCards();
      if (this.pileType === PileType.Stock && this.pile.hasCards) {
        this.prepareStock(isRefresh);
      }
    }
  }

  private prepareStock(isRefresh: boolean) {
    let cardCount = 0;

    if (Data.game) {
      switch (Data.game.stockClick) {
        case StockClickType.AddOneToWaste:
          cardCount = 1;
          break;
        case StockClickType.AddThreeToWaste:
          cardCount = 3;
          break;
      }
      // console.log(`prepareStock: stockClick=${StockClickType[Data.game.stockClick]}, cardCount=${cardCount}`);

      if (cardCount > 0 && this.pile) {
        this.pile.moveTopCard(Data.waste, true, cardCount);
      }
    }

    if (isRefresh) {
      Data.waste.component.prepare();
    }
  }

  checkForCards(): boolean {
    const cardCount: number = this.pile.cardCount;

    if (cardCount > 0) {
      // console.log(this.pile.pileTypeName + " fanstyle=" + FanType[this.pile.fanStyle]);
      this.setCardLocation();
      return true;
    }

    return false;
  }

  private setCardLocation() {
    this.cards = this.pile.cards;
    let currentCard: Card;
    const mustFan = this.pile.fanStyle === PileFanType.Vertical;
    const faceDownGap: number = mustFan ? 7 : 0; // non-fanned cards must have their top set to align with cards below
    const faceUpGap: number = mustFan ? 15 : 0;

    // Set location of cards above bottom card
    const padding = 3;
    let top: number = padding; // let card appear slightly below top of pile margin
    let left: number = padding;
    let previousCard: Card = this.cards[0];
    for (let i = 1; i < this.pile.cardCount; i++) {
      currentCard = this.cards[i];
      top += (currentCard.isFaceUp && previousCard.isFaceUp) ? faceUpGap : faceDownGap;

      if (this.pile.fanStyle === PileFanType.TopThreeHorizontal
          && this.pile.indexOfFirstCardAdded >= 0
          && i > this.pile.indexOfFirstCardAdded) {
        left += 12;
      }

      currentCard.cssStyle = { 'top': `${top}px`, 'left': `${left}px`, 'position': 'absolute' };
      previousCard = currentCard;
    }
  }

  setBackgroundImage() {
    let extra = '';
    switch (this.pileType) {
      case PileType.Foundation:
        if (this.pile.foundationStartValue === 0) {
          this.pile.foundationStartValue = 1;
        }
        extra = this.pile.foundationStartValue.toString();
        break;
      case PileType.Stock:
        extra = StockEmptyType[Data.game.stockEmpty];
        break;
      case PileType.Tableau:
        extra = TableauEmptyType[Data.game.tableauEmpty];
        break;
    }

    let path = `../../assets/back/${this.pile.pileTypeName}${extra}.png`;
    path = path.toLowerCase();
    // console.log(this.pile.pileTypeName + " pile image: " + path);
    this.cssStyle = { 'background-image': `url(${path})` };
  }

  prepareInitialPile() {
    if (this.pile) {
      this.pileType = this.pile.pileType;
    }
    else {
      // console.log("pile null, pileType=" + this.pileType);
      if (this.pileType === PileType.Stock) {
        this.pile = Data.stock;
      }
      else if (this.pileType === PileType.Waste) {
        this.pile = Data.waste;
      }
      else {
        this.pileType = PileType.Deck;
      }
    }

    if (this.pileType !== PileType.Deck && this.pile) {
      this.cssClass = PileType[this.pile.pileType].toLowerCase();
      this.setBackgroundImage();
    }
  }

  pileClick() {
    if (!this.wasClickHandled) {
      // This was not fired because a card on the pile was clicked
      const fn = `pileClick (${this.pile.pileTypeName}): `;
      if (this.pile.cardCount === 0) {
        // If there is a card on this pile, the card click event would have already been handled
        // This event should only handle an empty pile
        const selectedCard = Data.selectedCard;
        const what: string = selectedCard ? `selected card=${selectedCard.displayName}` : 'card not selected';
        // console.log(`${fn}${what}`);
        if (selectedCard) {
          // console.log("Checking if selected card can be added to empty pile");
          // Card is selected, check if it can be added here
          if (this.pile.checkIfCardCanBeAddedToEmptyPile(selectedCard)) {
            selectedCard.component.moveSelectedCard(this.pile);
          }
        }
        else if (this.pileType === PileType.Stock) {
          // Move cards from waste to stock
          // console.log(`${fn}moving cards from waste`);
          this.moveCardsFromWaste();
          this.prepare(true);
        }
      }
    }
    this.wasClickHandled = false; // reset in case pile is actually clicked
    // else {
    //   console.log(`${fn}card already handled`);
    // }
  }

  private moveCardsFromWaste(): void {
    const fn = 'moveCardsFromWaste: ';
    const wastePile: Pile = Data.waste;
    // console.log(`${fn}waste card count: ${wastePile.cardCount}`);

    const faceUp = false;
    while (wastePile.cards.length > 0) {
      wastePile.moveTopCard(this.pile, faceUp);
    }

    wastePile.indexOfFirstCardAdded = -1; // reset to prevent fanning issues
    // console.log(`${fn}all cards added to stock`);
  }
}
