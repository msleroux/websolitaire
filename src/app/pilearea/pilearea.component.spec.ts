import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PileareaComponent } from './pilearea.component';

describe('PileareaComponent', () => {
  let component: PileareaComponent;
  let fixture: ComponentFixture<PileareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PileareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PileareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
