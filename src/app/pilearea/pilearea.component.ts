import { Component, OnInit, Input } from '@angular/core';
import { Pile } from '../game/pile';
import { PileType } from '../game/enum';
import { Data } from '../game/data';

@Component({
  selector: 'app-pilearea',
  templateUrl: './pilearea.component.html',
  styleUrls: ['./pilearea.component.css']
})
export class PileareaComponent implements OnInit {
  @Input() pileType: PileType;

  piles: Pile[];
  cssClass = '';

  constructor() { }

  ngOnInit() {
    const type = PileType[this.pileType].toLowerCase();
    this.cssClass = `${type}Area`;
    this.setData();
  }

  private setData() {
    switch (this.pileType) {
      case PileType.Foundation:
        this.piles = Data.foundations;
        break;
      case PileType.Tableau:
        this.piles = Data.tableaus;
        break;
    }
  }
}
